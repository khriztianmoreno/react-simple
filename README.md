# React Simple Config

Create React App es una muy buena alternativa cuando queremos empezar un nuevo proyecto, para la mayoría de estos, nos basta con la configuración inicial de esta herramienta. Pero si queremos una aplicación más simple, o si queremos tener una configuración más personalizada, debemos conocer cómo configurar nuestro proyecto.


## Primeros pasos
Lo primero que debemos hacer es crear una nueva carpeta donde vamos a crear nuestro proyecto, he iniciar nuestra aplicación con `npm init` y responder a las preguntas que se nos muestran en consola, si queremos obviar estas preguntas ejecutamos `npm init -y`.

```bash
his utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (react-simple)
version: (1.0.0)
description:
entry point: (index.js)
test command:
git repository:
keywords:
author: Cristian Moreno <khriztianmoreno@gmail.com>
license: (ISC) MIT
About to write to /Users/khriztianmoreno/Projects/make_it_real/react-simple/package.json:

{
  "name": "react-simple",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Cristian Moreno <khriztianmoreno@gmail.com>",
  "license": "MIT"
}


Is this OK? (yes) yes
```

El anterior es un ejemplo de una configuración inicial, y la cual usamos para crear este nuevo proyecto.

**Para tener en cuenta**: algunas de las preguntas tienen valores asignados por defecto, los valores que vemos dentro de los paréntesis. Si queremos conservar estos valores, simplemente debemos darle Enter.

Esta configuración crea un archivo `package.json` con el siguiente contenido:


```json
{
  "name": "react-simple",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo ",
  },
  "author": "Cristian Moreno <khriztianmoreno@gmail.com>",
  "license": "MIT"
}

```

## Babel
Primero debemos instalar, como dependencia de desarrollo, babel. Esto lo hacemos haciendo uso del siguiente comando:

```bash
npm i -D add babel-core
```

Esta dependencia sera la encargada de transformar nuestro código, de ECMAScript6 a ECMAScript5 para que sea soportado por todos los navegadores. Además, nos va a permitir crear un archivo de configuración de babel en nuestra raíz llamado .babelrc, y en este archivo agregaremos la siguiente configuración (para ver las posibles opciones visita babeljs.io):

```
{
  "presets": [
    "env",
    "react"
  ],
  "plugins": [
    "react-hot-loader/babel"
  ]
}
```

Los presets son las configuraciones de babel que usaremos para transformas nuestro código. Para instalar los presets anteriores corremos el siguiente comando:

```bash
npm i -D babel-preset-env babel-preset-react
```

**env:** es una mezcla de las configuraciones de `babel-preset-es2015`, `babel-preset-es2016`, y `babel-preset-es2017` juntas.

**react:** nos permite utilizar toda la sintaxis relacionada con react y con jsx.

Y los plugins son extensiones de la funcionalidad de babel. En este caso instalamos `react-hot-reloader`, que nos permite hacer cambios en nuestros componentes de react sin necesidad de recargar nuestra página. Para instalarlo corremos el comando:

```bash
npm i -D react-hot-loader 
```


## Webpack
Por si solo babel no hace nada, para poder hacerlo funcionar vamos a tener que configurar webpack, que actualmente está en la versión 4. Para esto instalamos webpack.

```bash
npm i -D webpack
```

creamos un archivo llamado `webpack.config.js` y agregamos el siguiente contenido:

```js
const webpack = require('webpack');
module.exports = {
  entry: ['react-hot-loader/patch', './src/index.js'],
  output: {
    filename: 'bundle.[hash].js',
    publicPath: '/',
  },
  devtool: 'inline-source-map'
};
```

En esta configuración inicial lo que haremos es lo siguiente:

- En la primera linea importamos webpack.
- Luego en nuestra configuración agregamos `entry` para definir el punto de entrada de nuestra aplicación. En nuestro caso, el punto de entrada sera el plugin `react-hot-loader` y el archivo `index.js` que se encuentra en la carpeta `src`.
- Creamos en nuestra proyecto
- Luego definimos donde se van a crear la carpeta y archivos finales. En `output` definimos que el nombre del archivo, `filename`, sera `bundle.[hash].js`, donde `[hash]` es un hash auto-generado. Y además le decimos que cree la carpeta `dist` en la raíz de nuestra aplicación. En esta carpeta se crearan todos los archivos que son transformados al pasar por babeljs.
- Por último, agregamos `devtool: 'inline-source-map'`. Esto nos va a permitir usar las herramientas del desarrollador para depurar nuestra aplicación.


### Módulos
Ahora para continuar con nuestra configuración, vamos a agregar nuestros módulos. Los módulos se encargan de tareas especificas, en cada uno de nuestros módulos le vamos a decir a nuestra aplicación que hacer con ciertos tipos de archivo. Para esto agreguemos la siguiente configuración de módulos a nuestro archivo de configuración de webpack:

```js
module.exports = {
  ...
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      }
    ]
  },
  ...
}
```

Cada una de estas reglas es la que se encarga de cargar nuestros archivos al punto de salida. Nuestra primera regla va a cargar los archivos `.js`, excluyendo los que se encuentren en la carpeta `node_modules`, usando un loader que nos permite transpilar nuestro código usando babel `babel-loader`.

Para cada regla podemos usar más de un `loader`. Por ejemplo, en nuestra segunda regla, en la que cargaremos nuestros `.css`, estamos usando `style-loader` y `css-loader`. Además podemos agregar configuraciones extra para cada uno de estos `loaders`. Como vemos en `css-loader`, agregamos la llave `options` y tres configuraciones, `sourceMaps` activa el source map para nuestro css.

Cada uno de estos `loaders` debemos agregarlos a nuestro proyecto como dependecias de desarrollo. Con el siguiente comando agregaremos los usados anteriormente.


```bash
npm i -D babel-loader css-loader style-loader
```

Y para tener en cuenta, no solo existen estos 2 tipos de loaders, existen muchos más como: `json-loader`, `html-loader`, `file-loader`, etc. Acá podran ver un [listado completo](https://webpack.js.org/loaders/).


### Plugins
En los plugins agregaremos configuraciones adicionales de webpack y otras, con el siguiente código:

```js
module.exports = {
  ...
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      favicon: 'public/favicon.ico'
    })
  ],
  ...
}
```

- **NamedModulesPlugin**: nos mostrara la ruta relativa a un modulo que estemos importando.
- **HotModuleReplacementPlugin**: activa HMR, inserta cada modulo en tiempo de ejecución haciendo que la página se recargue automáticamente con cualquier cambio.
- **NoEmitOnErrorsPlugin**: nos permite detener la etapa de emisión de webpack cuando hay errores en nuestro código.
- **HtmlWebpackPlugin**: permite simplificar la creación de archivos HTML en el bundle. En este agregamos la ruta a nuestro archivo inicial de HTML y a nuestro favicon.

Como podrán haber notado, este es el único plugin que estamos utilizando que no es propio de webpack, por eso debemos instalarlo como dependencia de desarrollo y requerirlo en nuestro código.

```bash
npm i -D html-webpack-plugin
```

Y lo requerimos de la siguiente manera al inicio de nuestro archivo de configuración:

```js
const HtmlWebpackPlugin = require('html-webpack-plugin');
```

Acá podrán ver una lista de los [plugins de webpack](https://webpack.js.org/plugins/).


### DevServer
Nos permite montar nuestra aplicación en localhost para desarrollar de una manera más rápida y efectiva. Como siempre primero instalamos las dependencias necesarias:

```bash
npm i -D webpack-dev-server
```

Y luego agregamos nuestra configuración:

```js
module.exports = {
  ...
  
  devServer: {
    host: 'localhost',
    port: 3000,
    historyApiFallback: true,
    open: true,
    hot: true
  }
}
```

- **host**: definimos el host en el que va a correr `devServer`.
- **port**: definimos el puerto en el que va a correr devServer.
- **historyApiFallback**: muestra nuestro index.html cuando el servidor responde con un `404`.
- **open**: abre el navegador cuando iniciamos nuestra aplicación.
- **hot**: para habilitar Hot Module Replacement.

Acá podrán encontrar opciones adicionales para [configurar devServer](https://webpack.js.org/configuration/dev-server/).

No olvides crear una carpeta `public` y en ella agregar un `HTML` básico y un favicon. Para nuestro ejemplo usaremos el siguiente `HTML`:

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>New React App</title>
</head>
<body>
  <div id="app"></div>
</body>
</html>
```

### Scripts

Agregaremos tres scripts básicos que nos permitirán iniciar nuestra aplicación, crear un build en modo desarrollo y un build en modo producción. Webpack 4 agrego los modos, en estos podemos definir en que modo queremos correr algún script y optimizar las tareas que corre webpack para cada uno de ellos. Los scripts son los siguientes:

```
"scripts": {  
  "build:production": "webpack --mode production",
  "build:development": "webpack --mode development",
  "start": "webpack-dev-server --mode development"
}
```

Para que estos scripts funcionen agregamos a nuestro proyecto la siguiente dependencia:

```bash
npm i -D wepback-cli
```


## React
Por último, lo más importante, agregaremos react a nuestro proyecto. Para esto agregamos react y react-dom a nuestras dependencias con el siguiente comando:

```bash
npm i -S add react react-dom
```

De acá en adelante es terreno conocido por lo que no explicare mucho.

Cremos una carpeta `src` y un archivo `index.js` y otro `App.js`.

En `index.js` agregamos el siguiente contenido:

```js
import React from 'react';
import ReactDom from 'react-dom';

import App from './App.js';

const app = document.getElementById('app');

ReactDom.render(<App />, app);
```

Y en `App.js` el siguiente contenido:

```js
import React from 'react';

const App = () => (
  <h1>Lo hicimos!!</h1>
);

export default App;
```

### Run
Hemos terminado con toda nuestra configuración, con la practica esta tarea se va haciendo más fácil. Ahora corre `npm start` para ver el resultado.

```bash
npm start 
```
